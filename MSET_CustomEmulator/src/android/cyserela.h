#ifndef _CYSERELA_H_INCLUDED_
#define _CYSERELA_H_INCLUDED_

#include <stddef.h>

// This file contains definitions and declarations used by the cybersec lab's
// custom emulator. (PS: I am sorry for how much of a mess the modified code is)
// Note: Other headers have been modified to make the emulator work

#define INI_FILEPATH "cyserela-config.ini"

#define INI_IMSI_KEY "imsi"
#define INI_IMEI_KEY "imei"
#define INI_SIM_KEY  "sim"

// Both of these values have to be 15 characters long strings
#define DEFAULT_IMSI "123456789012345"
#define DEFAULT_IMEI "986543210654321"

// this value must be a 20 characters long string
#define DEFAULT_SIM_SERIAL_NUMBER "01234567890123456789"

#define IMSI_LENGTH 15
#define IMEI_LENGTH 15
#define SIM_SERIAL_NUMBER_LENGTH  20

#define DEFAULT_RESPONSES_IMSI_INDEX 45
#define DEFAULT_RESPONSES_IMEI_INDEX 46
#define ANSWERS_SIM_INDEX 5


///// THE FOLLOWING IS FOR THE STRUCT WHICH CONTAINS IMSI/IMEI INFORMATION

/* a function used to deal with a non-trivial request */
typedef const char*  (*ResponseHandler)(const char*  cmd, int*  modem);

typedef struct DefaultResponse {
    const char*      cmd;     /* command coming from libreference-ril.so, if first
                                 character is '!', then the rest is a prefix only */

    const char*      answer;  /* default answer, NULL if needs specific handling or
                                 if OK is good enough */

    ResponseHandler  handler; /* specific handler, ignored if 'answer' is not NULL,
                                 NULL if OK is good enough */
} DefaultResponse;

#ifdef DEFINE_DefaultResponses
DefaultResponse sDefaultResponses[];
#else
extern DefaultResponse sDefaultResponses[];
#endif


///// THE FOLLOWING IS FOR THE STRUCT WHICH CONTAINS SIM INFORMATION

typedef struct AnswerStruct {
    const char*  cmd;
    const char*  answer;
} AnswerStruct;

#ifdef DEFINE_answers
AnswerStruct answers[];
#else
extern AnswerStruct answers[];
#endif


#endif // _CYSERELA_H_INCLUDED_
