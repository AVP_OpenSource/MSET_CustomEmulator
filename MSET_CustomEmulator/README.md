# Customized Google Emulator
## What is this?
This repository contains modified source files from the Google android emulator.

The customized emulator currently allows to:
 * set the IMEI, IMSI and SIM serial number via a configuration file.

It also currently has changed defaults for some values, namely:
 * phone number (is now 1222333[port digits], ex: 1 222 333 5554)
 * voicemail number (is now 67778910234)

## Configuration file
The Customized Google Emulator currently uses a configuration file, cyserela-config.ini, which is expected to be in the current working directory. These are the currently configurable options:
 * the **imei** property is the IMEI number. It must be 15 characters long.
 * the **imsi** property is the IMSI number. It must be 15 characters long.
 * the **sim** property is the SIM serial number. It should be 20 characters long (not tested with other lengths).

**The configuration file format is [ini](https://en.wikipedia.org/wiki/INI_file#Format).**

## Spotting the changes we made
In order to find the changes we made in the C code, search for comments such as:

    //<avp-custom-modifications>
    ...
    //</avp-custom-modifications>
Such comments delimit our changes.
Changes were also made in the android-configure.sh script, where we simply added  -modified  to built files extension (though it currently doesn't really work), try searching for that if you need.

# Note
Those changes were applied to the following branch of the emulator source code: https://android.googlesource.com/platform/external/qemu/+/studio-1.4-dev
