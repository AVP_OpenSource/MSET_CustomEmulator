##### BEFORE WE BEGIN
There should be a file distributed with this one named DEVELOPMENT.TXT; it's
the file I followed to build the emulator. If you don't succeed by following
my instructions, try following the ones in that file instead and adapting them
to your needs.
This file is basically a more verbose, annotated version of that other file.

## COMPILING QEMU ANDROID EMULATOR

##### VM FOR BUILDS ##
Create yourself a Linux VM if you don't want to fill your computer with crap..
Tested using LUbuntu 15 64bit, but should work with regular Ubuntu as well.
Allow your VM to have enough RAM and a big enough harddrive: here's what I got -> RAM: 4096Mb, HD: 50Gb
This might be overkill (or not), but better give more than less... Consider
also giving it enough CPU power (I use VirtualBox and by default it makes it's
VMs very limited in terms of processing power, VMWare might be more automagical)

## INSTALLING REQUIRED STUFF
Once your VM is ready, we need to install a couple things on it
In a terminal emulator, run:

    sudo apt-get install gcc gcc-multilib g++ g++-multilib make git scons

There might be more, and some of those might not be necessary, but since the
whole process of creating the build VM is REALLY long, I'm too lazy to make
sure.
Then, you need to get Google's 'repo' tool, which allows you to checkout
the sources... Don't know why they don't just go with git alone but whatever.
See https://source.android.com/source/downloading.html for download and
installation instructions.
I will be using the studio-1.4-dev branch for this, but you might need a more
recent one. You are on your own for that.
Checkout the sources in an appropriate folder:

    mkdir studio-dev
    cd studio-dev
    repo init -u https://android.googlesource.com/platform/manifest -b studio-1.4-dev

This should take a very long time, so go watch a movie or something... Also
make sure to specify the branch! with (-b) I didn't the first time and all I
ended up with is tons of source code of stuff I don't care about and
prebuilt emulators!

## BUILDING THE EMULATOR
Apparently, we have to build some dependencies to get to whole thing to work
but Google is kind enough to give us scripts that makes them automatically.

    cd studio-dev/external/qemu
    android/scripts/build-qemu-android.sh

This will be long too, but not dependent of your internet speed, at least...

##### NOTE:
There's another dependency building script: android/scripts/build-mesa.sh
It failed when I tried to build it, but the emulator(s) still compiled after
that..
I think this dependency has something to do with the graphical rendering, and
I might have already installed the dependencies with apt-get, which could
explain why the builds succeeded without it.
I quote: "For your information, the Mesa library is used to implement
software-based GLES emulation, and 'qemu-android' is the new emulator code
based on a more recent upstream QEMU..."


### Finally, the emulator binaries may be built:

    cd studio-dev/external/qemu
    ./android-rebuild.sh

This is also long...

By default, the compiled emulator binaries will be in 
    
    studio-dev/external/qemu/objs/


### TIPS AND TICKS
#### VirtualBox GUI, Host Only network for VM:
For that to work, you have to manually create a Host-Only network
File -> Preferences -> Network tab -> Host-only Networks
click on the green + . You now have a Host-only network!
Go to your VM's settings and add that new network to it's networks
#### Other sutff
Make sure you have a decent way to transfer files between your build VM and
your computer. You can use shared folders. In my case, LUbuntu didn't want a shared folder with my host, so I used the rsync command instead (I run linux as my host)

I recommend controlling your build machine through ssh. Install openssh-server on your VM:

    sudo apt-get install openssh-server

then connect to it with putty (on Windows) or the ssh command.

# Not working? Google is your friend
# Good luck

